<html>
	<head>
		<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css" />
		<link rel="stylesheet" type="text/css" href="font-awesome/css/font-awesome.min.css" />
		<link rel="stylesheet" type="text/css" href="css/util.css">
		<link rel="stylesheet" type="text/css" href="css/main.css">
		<link rel="stylesheet" type="text/css" href="css/local.css" />
		<link rel="stylesheet" type="text/css" href="css/tableMine.css" />
		<script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="js/jquery-3.3.1.js"></script>
		<script type="text/javascript" src="js/transition.js"></script>   
		<link rel="stylesheet" type="text/css" href="css/stylin.css">
	</head>
	
	<body>
		<div class="container-login100" id="wrapper" style="background-image: url('images/bg-02.jpg');">
			 <table id=patient style="width:100%">
			  <tr>
				<th>Training ID</th>
				<th>Training Title</th>
				<th>Training Type</th>
				<th>Training Venue</th>
				<th>Training Date</th>
				<th>Trainer</th>
				<th>Trainer Email</th>
			  </tr>
			  
			  <?php
				include("config.php");
				session_start();
				
				$sql="SELECT * FROM Trainings WHERE MONTH(TrainingDate) = 7";
				$result=mysqli_query($conn, $sql);
				if(mysqli_num_rows($result)>0)
				{
					while($row=mysqli_fetch_array($result))
					{
						echo"<tr>";
						echo"<td>".$row['TrainingID']."</td>";
						echo"<td>".$row['TrainingTitle']."</td>";
						echo"<td>".$row['TrainingType']."</td>";
						echo"<td>".$row['TrainingVenue']."</td>";
						echo"<td>".$row['TrainingDate']."</td>";
						echo"<td>".$row['Trainer']."</td>";
						echo"<td>".$row['TrainerEmail']."</td>";
						echo"</tr>";
					}
				}
				?>
			</table> 
		</div>
		</body>
</html>