<html>
	<head>
		<meta charset="UTF-8">
		<link rel="stylesheet" type="text/css" href="css/tablestyle.css">
		<link rel="stylesheet" type="text/css" href="font-awesome/css/font-awesome.min.css" />
		<link rel="stylesheet" type="text/css" href="css/main.css">
		<script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="js/jquery-3.3.1.js"></script>
		<script type="text/javascript" src="js/transition.js"></script>   
		<link rel="stylesheet" type="text/css" href="css/stylin.css">
		</head>
		<style>
			div
			{
				padding-bottom:-10px;
			}
		</style>
		
	<body>
		<div>
			<div class="table-users" style="background-image: url('images/bg-02.jpg');">
				<div class="header">Trainings In July</div>
						<table id="example" class="display" style="width:100%">
							<tr>
				<th>Training ID</th>
				<th>Training Title</th>
				<th>Training Type</th>
				<th>Training Venue</th>
				<th>Training Date</th>
				<th>Trainer</th>
				<th>Trainer Email</th>
			  </tr>
			  
				<?php
					include("config.php");
					session_start();
					
					$sql="SELECT * FROM Trainings WHERE MONTH(TrainingDate) = 7";
					$result=mysqli_query($conn, $sql);
					if(mysqli_num_rows($result)>0)
					{
						while($row=mysqli_fetch_array($result))
						{
							echo"<tr>";
							echo"<td>".$row['TrainingID']."</td>";
							echo"<td>".$row['TrainingTitle']."</td>";
							echo"<td>".$row['TrainingType']."</td>";
							echo"<td>".$row['TrainingVenue']."</td>";
							echo"<td>".$row['TrainingDate']."</td>";
							echo"<td>".$row['Trainer']."</td>";
							echo"<td>".$row['TrainerEmail']."</td>";
							echo"</tr>";
						}
					}
				?>
						</table>
						<a href="#" onclick="HTML to PDF()"></a>
						<script src="js/jspdf.js"></script>
				</div>
				
				<div class="table-users" style="background-image: url('images/bg-02.jpg');">
				<div class="header">Members of IT Department</div>
					<table id="example" class="display" style="width:100%">
						<tr>
						<th>Employee Number</th>
						<th>Employee Name</th>
						<th>Employee Age</th>
						<th>Employee Address</th>
						<th>Employee Phone Number</th>
						<th>Employee Gender</th>
						<th>Department</th>
						<th>Employee Station</th>
						
						</tr>
						  
						<?php								
							$sql="SELECT * FROM staffmembers WHERE Department = 'IT' ";
							$result=mysqli_query($conn, $sql);
							if(mysqli_num_rows($result)>0)
							{
								while($myrow=mysqli_fetch_array($result))
								{
									echo"<tr>";
									echo"<td>".$myrow['EmployeeNumber']."</td>";
									echo"<td>".$myrow['EmployeeName']."</td>";
									echo"<td>".$myrow['EmployeeAge']."</td>";
									echo"<td>".$myrow['EmployeeAddress']."</td>";
									echo"<td>".$myrow['EmployeePhoneNumber']."</td>";
									echo"<td>".$myrow['EmployeeGender']."</td>";
									echo"<td>".$myrow['Department']."</td>";
									echo"<td>".$myrow['EmployeeStation']."</td>";
									echo"</tr>";
								}
							}
						?>
					</table>
				</div>
				
				<div class="table-users" style="background-image: url('images/bg-02.jpg');">
				<div class="header">Members of HR Who attended CPA Training</div>
					<table id="example" class="display" style="width:100%">
						<tr>
						<th>Employee Number</th>
						<th>Employee Name</th>
						<th>Employee Age</th>
						<th>Employee Address</th>
						<th>Employee Phone Number</th>
						<th>Employee Gender</th>
						<th>Department</th>
						<th>Employee Station</th>
						
						</tr>
						  
						<?php								
							$sql="SELECT * FROM staffmembers INNER JOIN traininglog
							ON staffmembers.EmployeeNumber= traininglog.EmployeeNumber WHERE staffmembers.Department = 'Human Resource Management' ";
							$result=mysqli_query($conn, $sql);
							if(mysqli_num_rows($result)>0)
							{
								while($thisrow=mysqli_fetch_array($result))
								{
									echo"<tr>";
									echo"<td>".$thisrow['EmployeeNumber']."</td>";
									echo"<td>".$thisrow['EmployeeName']."</td>";
									echo"<td>".$thisrow['EmployeeAge']."</td>";
									echo"<td>".$thisrow['EmployeeAddress']."</td>";
									echo"<td>".$thisrow['EmployeePhoneNumber']."</td>";
									echo"<td>".$thisrow['EmployeeGender']."</td>";
									echo"<td>".$thisrow['Department']."</td>";
									echo"<td>".$thisrow['EmployeeStation']."</td>";
									echo"</tr>";
								}
							}
						?>
					</table>
				</div>
				
		</div>
	</body>
</html>